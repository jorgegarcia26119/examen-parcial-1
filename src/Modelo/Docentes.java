/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

/**
 *
 * @author _Enrique_
 */
public class Docentes {
    
    int numDocente, nivel, horas;
    String nombre,domicilio;
    float pagoHora;
    
    public Docentes(){
        
        this.numDocente=0;
        this.nombre="";
        this.domicilio="";
        this.nivel=0;
        this.pagoHora=0.02f;
        this.horas=0;
    }
    
    public Docentes(Docentes docen){
        
        this.numDocente=docen.numDocente;
        this.nombre=docen.nombre;
        this.domicilio=docen.domicilio;
        this.nivel=docen.nivel;
        this.pagoHora=docen.pagoHora;
        this.horas=docen.horas;
    }
    
    public Docentes(int numD, String nombre, String domicilio, int nivel, float pagoH, int horas){
        
        this.numDocente=numD;
        this.nombre=nombre;
        this.domicilio=domicilio;
        this.nivel=nivel;
        this.pagoHora=pagoH;
        this.horas=horas;
        
    }

    public int getNumDocente() {
        return numDocente;
    }

    public void setNumDocente(int numDocente) {
        this.numDocente = numDocente;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    public float getPagoHora() {
        return pagoHora;
    }

    public void setPagoHora(float pagoHora) {
        this.pagoHora = pagoHora;
    }
    
    public float calcularPago(){
    
        float total=0.0f;
        if(nivel==1){
            
            total = (this.pagoHora + (this.pagoHora/100)*30)*this.horas;
            
        }
        else if(nivel==2){
            
            total = (this.pagoHora + (this.pagoHora/100)*50)*this.horas;
            
        }
        else{
            
            total = (this.pagoHora*2)*this.horas;
        }
        return total;
    }
    
    public float calcularImpuesto(){
        
        float totalImp=0.0f;
        
        totalImp=(calcularPago()/100)*16;
        return totalImp;
        
    }
    
    public float calcularBono(int bono){
        float totalBono =0.0f;
        
        return bono;
    }
    
    
}




