/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Docentes;
import Vista.dlgDocentes;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

/**
 *
 * @author _Enrique_
 */
public class Controlador implements ActionListener{
    
    private dlgDocentes Vista;
    private Docentes Docentes;
    
    public Controlador(dlgDocentes Vista){
        
        this.Vista=Vista;
      
        //hacer que el controlador escuche los botones de la vista
        Vista.btnNuevo.addActionListener(this);
        Vista.btnGuardar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        Vista.btnMostrar.addActionListener(this);
        Vista.btnCerrar.addActionListener(this);
        Vista.btnLimpiar.addActionListener(this);
        
    }
    
        
       private void iniciarVista(){
    
        Vista.setTitle("Productos");
        Vista.setSize(800,800);
        Vista.setVisible(true);
        
    }  
       
    @Override
    public void actionPerformed(ActionEvent e) {
     
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if(e.getSource()==Vista.btnNuevo){
            
            Vista.txtNombre.setEnabled(true);
            Vista.txtDomicilio.setEnabled(true);
            Vista.txtNumD.setEnabled(true);
            Vista.txtHoras.setEnabled(true);
            Vista.txtPagoH.setEnabled(true);
            
            Vista.cmbNivel.setEnabled(true);
            
            Vista.btnCerrar.setEnabled(true);
            Vista.btnLimpiar.setEnabled(true);
            Vista.btnGuardar.setEnabled(true);
            Vista.btnMostrar.setEnabled(true);
            
        }
        if(e.getSource()==Vista.btnCerrar){
          int option=JOptionPane.showConfirmDialog(Vista,"¿Deseas salir?",
          "Decide", JOptionPane.YES_NO_OPTION);
           if(option==JOptionPane.YES_NO_OPTION){
               Vista.dispose();
               System.exit(0);

           }
            
        }
        if(e.getSource()==Vista.btnGuardar){
            
            try{
                
                
                Docentes.setNombre(Vista.txtNombre.getText());
                
                
            }catch(NumberFormatException ex){
                JOptionPane.showMessageDialog(Vista, "Surgio el siguiente error: "+ex.getMessage());
            
                
            }
        }
          
          
    }
}
